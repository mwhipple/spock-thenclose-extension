#########################
spock-thenclose-extension
#########################

An extension for Spock which allows running some code implicitly at the end of a feature.

This extension was designed to solve a specific problem which is outlined here. The extension
itself could be used for other purposes and more general documentation will be provided.

************
Installation
************

Gradle
======

.. code-block:: groovy

   repositories {
       maven {
       	   //jcenter linkaage should be forthcoming
           url  "https://dl.bintray.com/mwhipple/spock-thenclose-extension" 
       }
   }

   dependencies {
       testCompile 'com.mattwhipple:spock-thenclose-extension:0.2.0'
   }

Others
======

Whatever they do that's equivalent.

*********************
Quick Spielless Usage
*********************

1. Extend ``ThenkfulSpecification`` instead of vanilla ``Specification``.
2. Write assertions within Mock closures like ``thenk { assert it }``
3. Profit

************************
Mock Argument Assertions
************************

The Issue
=========

Spock has some of the coolest mocking around, but it's not one of those magical bundles of logic
that's great out-of-the box at doing everything at once (if you know of any of these please let me know).
Specifically, the standard mocking functionality largely revolves around pattern matching which
makes the use of the mocks/stubs a breeze. It unfortunately can also lead to less than helpful
feedback when the pattern doesn't match quite right, which is aggravated by the likely use of
more complex pattern rules to attempt to do things like verify the arguments passed to a mock.

This can lead to the power assert-less potentially less than helpful error messages like:

.. code-block:: groovy

   Too few invocations for:

   1 * worker.doSomething({it.content == 'foo' && it.id})   (0 invocations)

   Unmatched invocations (ordered by similarity):

   1 * worker.doSomething(Message@71bfc0b4)


The Workaround
==================

The above feedback is less than optimally helpful because it is conflating the
pattern matching behavior with the desire to do an assertion. In cases where we
can assume that a method is going to be called and we want to assert that the
arguments passed are correct, it is better to split the pattern (the method) from
the assertion. Spock's support for Closures with Mocks makes this simple:

.. code-block:: groovy

   1 * worker.doSomething(*_) >> { args ->
     assert args[0].content == 'foo'
     assert args[0].id

     myReturnValue
   }

Now the feedback will bring attention to specific differences using the usual power assert format.

The Issue With the Workaround
=============================

The workaround above introduces a new lurking problem. The assertion is performed when the
Mock is called rather than in the Spock test suite. This can lead to errors getting
squelched in the unfortunate event that the code under test has some kind of call like:

.. code-block:: groovy

   try {
     worker.doSomething(myWrongArgs)
   } catch (Throwable t) {
     // swallow errors
   }

``AssertionError``\ s will blow right through ``Exception`` catching blocks which mean that the
vast majority of code is safe, but the (questionable?) code that catches ``Throwable``\ s could
cause tests that should fail to pass. Although catching ``Throwable``\s should likely be avoided,
the issue remains that the tests should still work and this circles back to the fact that the
assertion is being done in the code under test rather than in the test framework.

A Workaround for the Workaround
===============================

A solution to prevent the assertions from being swallowed is to make sure they are
performed in the test case. This can be accomplished through the use of a stashing
the assertion in a closure which can be later evaluated in the test case (using a
`thunk <https://en.wikipedia.org/wiki/Thunk>`_).
The general pattern for this approach would look something like:

.. code-block:: groovy

   given:
   def thunks = []

   when:
   myCode.doWork('foo')

   then:
   1 * worker.doSomething(*_) >> { args ->
     thunks << {
       assert args[0].content == 'foo'
       assert args[0].id
     }

     myReturnValue
   }
   thunks*.call()

This allows capturing of the assertion values in the closure but deferring the actual
evaluation until control is squarely back in Spock's hands.

Packaging the Workaround Workaround
===================================

The above approach works well, but if you want to use the pattern frequently it
introduces a fair amount of boilerplate: that's where this extension fits in.
Using this extension the above code can be written as:

.. code-block:: groovy

   class MySpec extends ThenkfulSpecification {
       def 'some work should call some stuff'() {
           when:
           myCode.doWork('foo')

           then:
           1 * worker.doSomething(*_) >> { args ->
               thenk { assert args[0].content == 'foo' }
               thenk { assert args[0].id }

               myReturnValue
            }
        }
    }

*(Whether to group asserts together or use separate lines is discretionary.)*
The above marks the closure as a thunk to be evaluted in the equivalent of the
``then`` block ("then" + "thunk" = "thenk").

************
How it Works
************

ThenCleanup
===========

The one tricky bit in packaging the above logic for reuse is the implicit evaluation
of the thunks. This is handled by a Spock extension (for which this is the repository)
which allows code to be executed immediately after the code in the feature. This is
the ``@ThenCleanup`` extension. The extension itself is largely based on ``@AutoCleanup``
with modifications as necessary to evaluate the thunks as close to the feature execution
as possible (rather than during a cleanup step).

SuperClass
==========

With the extension, the rest of the logic is straightforward additions to a base class.
The thunks are stored in an instance variable which is marked with the ``ThenCleanup`` and
a `thenk` method is added to the base class which collects provided closures into the
collection of thunks.

*************
The Extension
*************

TODO
