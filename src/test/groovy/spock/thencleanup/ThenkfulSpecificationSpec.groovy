package spock.thencleanup

import java.util.function.Predicate

import groovy.transform.TupleConstructor

import spock.lang.PendingFeature
import spock.lang.Specification

@TupleConstructor
class QuietablePredicate<T> implements Predicate {
    @Delegate Predicate<T> p
    Boolean quieted = false

    boolean test(T t) {
        try {
	    p.test(t)
	} catch (Throwable e) {
	    if (!quieted) throw e
	}
    }
}

// Use @PendingFeature for expected failures

class ThenklessSpecificationSpec extends Specification {
    Predicate mp = Mock(Predicate)
    QuietablePredicate qp = new QuietablePredicate(mp)

    @PendingFeature
    def 'Failed standard pattern match fails'() {
	when:
	qp.test('blah')

	then:
	1 * mp.test('bla') >> true
    }

    def 'Passing assertion passes'() {
	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    assert args[0] == 'blah'
	    true
	}
    }

    @PendingFeature
    def 'Unswallowed failed assertion fails'() {
	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    assert args[0] == 'b'
	    true
	}
    }

    def 'Swallowed failed assertion passes'() {
    	given:
	qp.quieted = true	

	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    assert args[0] == 'b'
	    true
	}
    }
}

class ThenkfulSpecificationSpec extends ThenkfulSpecification {
    Predicate mp = Mock(Predicate)
    QuietablePredicate qp = new QuietablePredicate(mp)

    def 'Passing assertion passes'() {
	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    thenk{ assert args[0] == 'blah' }
	    true
	}
    }

    @PendingFeature
    def 'Unswallowed failed assertion fails'() {
	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    thenk{ assert args[0] == 'b' }
	    true
	}
    }

    @PendingFeature
    def 'Swallowed failed assertion fails'() {
    	given:
	qp.quieted = true	

	when:
	qp.test('blah')

	then:
	1 * mp.test(*_) >> { args ->
	    thenk{ assert args[0] == 'b' }
	    true
	}
    }

    @PendingFeature
    def 'Swallowed failed assertion fails in parameterized test'() {
        given:
	qp.quieted = true

	when:
	qp.test(a)

	then:
	1 * mp.test(*_) >> { args ->
	    thenk{ assert args[0] == 'b' }
	    true
	}

	where:
	a << ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    }

}
