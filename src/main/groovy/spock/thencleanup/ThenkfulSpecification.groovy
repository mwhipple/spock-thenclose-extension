package spock.thencleanup

import java.util.concurrent.ConcurrentLinkedQueue

import groovy.transform.CompileStatic

import spock.lang.Specification

@CompileStatic
class Thunks implements Queue<Closure<?>> {
    @Delegate final ConcurrentLinkedQueue<Closure<?>> queue = new ConcurrentLinkedQueue<>()

    void think() {
        queue*.call()
    }
}

class ThenkfulSpecification extends Specification {
    @ThenCleanup('think') Thunks thunks = new Thunks()

    void thenk(Closure<?> f) { thunks << f }
}
