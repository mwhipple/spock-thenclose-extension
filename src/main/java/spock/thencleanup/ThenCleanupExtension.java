package spock.thencleanup;

import org.spockframework.runtime.extension.AbstractAnnotationDrivenExtension;
import org.spockframework.runtime.model.FeatureInfo;
import org.spockframework.runtime.model.FieldInfo;
import org.spockframework.runtime.model.SpecInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ThenCleanupExtension extends AbstractAnnotationDrivenExtension<ThenCleanup> {
    private List<FieldInfo> fields = new ArrayList<>();

    @Override
    public void visitFieldAnnotation(ThenCleanup annotation, FieldInfo field) {
	if (field.isShared()) 
	    throw new IllegalArgumentException("Shared fields are not supported by ThenCleanup");
	fields.add(field);
    }

    @Override
    public void visitSpec(SpecInfo spec) {
	Collections.reverse(fields);
	ThenCleanupInterceptor interceptor = new ThenCleanupInterceptor(fields);
	for (FeatureInfo feature: spec.getFeatures())
	    feature.addInterceptor(interceptor);
    }
}
