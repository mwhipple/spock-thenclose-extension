package spock.thencleanup;

import org.spockframework.runtime.GroovyRuntimeUtil;
import org.spockframework.runtime.extension.ExtensionUtil;
import org.spockframework.runtime.extension.IMethodInterceptor;
import org.spockframework.runtime.extension.IMethodInvocation;
import org.spockframework.runtime.model.FieldInfo;

import java.util.ArrayList;
import java.util.List;

public final class ThenCleanupInterceptor implements IMethodInterceptor {
    private final Innterceptor innterceptor;

    public ThenCleanupInterceptor(List<FieldInfo> fields) {
	this.innterceptor = new Innterceptor(fields);
    }

    @Override
    public void intercept(IMethodInvocation invocation) throws Throwable {
	invocation.getFeature().getFeatureMethod().addInterceptor(innterceptor);
	invocation.proceed();
    }

    final class Innterceptor implements IMethodInterceptor {
	private final List<FieldInfo> fields;

	Innterceptor(List<FieldInfo> fields) {
	    this.fields = fields;
	}

	@Override
        public void intercept(IMethodInvocation invocation) throws Throwable {
	    List<Throwable> exceptions = new ArrayList<>();

	    try {
		invocation.proceed();
	    } catch (Throwable t) {
		exceptions.add(t);
	    }
	    for (FieldInfo field : fields) {
		ThenCleanup annotation = field.getAnnotation(ThenCleanup.class);

		try {
		    Object value = field.readValue(invocation.getInstance());
		    if (value == null) continue;

		    GroovyRuntimeUtil.invokeMethod(value, annotation.value());
		} catch (Throwable t) {
		    if (!annotation.quiet()) exceptions.add(t);
		}
	    }
	    ExtensionUtil.throwAll(exceptions);
	}
    }
}
